#include "cloud_manager.h"

CloudManager::CloudManager()
    : ParticleManager()
    , screenWidth(0.0f)
    , screenHeight(0.0f)
{
}

void CloudManager::setScreenSize(const int &width, const int &height)
{
    screenWidth = static_cast<float>(width);
    screenHeight = static_cast<float>(height);
}

void CloudManager::setup()
{
        ResourceManager::loadShaderFromSource(R"(
        #version 330 core
        layout (location = 0) in vec2 aPos;

        out vec2 texCoords;
        out vec4 aColor;

        uniform mat4 projection;
        uniform vec2 offset;
        uniform vec4 color;

        void main()
        {
            float scale = 300.0f;
            texCoords = aPos;
            aColor = color;
            gl_Position = projection * vec4((aPos * scale) + offset, 0.0, 1.0);
        }
    )", R"(
        #version 330 core

        in vec2 texCoords;
        in vec4 aColor;

        out vec4 fragColor;

        uniform sampler2D sprite;

        void main()
        {
            fragColor = texture(sprite, texCoords) * aColor;
        }
    )", "cloud");

    GLfloat vertex_quad[] = {
        0.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    glGenVertexArrays(1, &VAO_);
    glGenBuffers(1, &VBO_);
    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_quad), vertex_quad, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), static_cast<GLvoid *>(0));
    glBindVertexArray(0);

    for (int i = 0; i < 10; ++i)
    {
        auto direction = (generateRandom(0, 1) == 0 ? -1.0f : 1.0f);
        clouds.emplace_back(glm::vec2(generateRandom(0.0f, screenWidth), generateRandom(0.0f, screenHeight)), randomVec2(3.0f, 5.0f), glm::vec4(glm::vec3(1.0f), 0.75f), 1.0f, direction);
    }
}

void CloudManager::update()
{
    for (auto &cloud : clouds)
    {
        cloud.position.x += cloud.direction * cloud.velocity.x;

        if (!(cloud.position.x < screenWidth && cloud.position.x > 0)
            && !(cloud.position.x + cloud.sprite.getWidth() < screenWidth && cloud.position.x + cloud.sprite.getWidth() > 0))
        {
            cloud.resetPosition(screenWidth, screenHeight);
        }
    }
}

void CloudManager::draw()
{
    auto shader = ResourceManager::getShader("cloud").use();
    for (auto cloud : clouds)
    {
        shader.setVector2f("offset", cloud.position);
        shader.setVector4f("color", cloud.color);
        cloud.sprite.bind();
        glBindVertexArray(VAO_);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }
}

void CloudManager::clear()
{
    glDeleteVertexArrays(1, &VAO_);
    glDeleteBuffers(1, &VBO_);
}