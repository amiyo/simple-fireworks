#pragma once

#include <GL/gl3w.h>
#include <glm/glm.hpp>

class Shader
{
public:
    Shader() : ID_(0) {}
    Shader(const GLchar *vertexSource, const GLchar *fragmentSource);

    Shader &use();
    void compile(const GLchar *vertexSource, const GLchar *fragmentSource);

    void setVector2f(const GLchar *name, GLfloat x, GLfloat y, GLboolean useShader = false);
    void setVector2f(const GLchar *name, const glm::vec2 &value, GLboolean useShader = false);
    void setVector4f(const GLchar *name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader = false);
    void setVector4f(const GLchar *name, const glm::vec4 &value, GLboolean useShader = false);
    void setMatrix4(const GLchar *name, const glm::mat4 &value, GLboolean useShader = false);

    GLuint getID() const;

private:
    GLuint ID_;

    enum Type
    {
        VERTEX = 0,
        FRAGMENT,
        PROGRAM
    };
    void checkCompileErrors(const GLuint &object, const Type &type);
};