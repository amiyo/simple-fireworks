﻿#pragma once

#include <list>

#include "utils.h"
#include "particle_manager.h"

class FireworkManager : public ParticleManager
{
public:
    FireworkManager();

    void setup() override;

    void spawn(const glm::vec2 &mousePosition, const GLfloat &screenHeight);
    void update() override;
    void draw() override;

    void clear() override;

private:
    struct Firework : public Particle
    {
        glm::vec2 mousePosition;

        struct Spark : public Particle
        {
            glm::vec2 direction;

            Spark() : Particle(), direction(randomVec2(-1.0f, 1.0f)) { }
            Spark(const glm::vec2 &position, const glm::vec2 &velocity, const glm::vec4 &color, const GLfloat &life)
                : Particle(position, velocity, color, life), direction(randomVec2(-1.0f, 1.0f)) { }
        };
        std::list<Spark> sparks;

        Firework() : Particle(), mousePosition() { }
        Firework(const glm::vec2 &position, const glm::vec2 &velocity, const glm::vec4 &color, const GLfloat &life, const GLfloat &mousePositionY)
            : Particle(position, velocity, color, life), mousePosition(position.x, mousePositionY) { }
    };
    std::list<Firework> fireworks;
};