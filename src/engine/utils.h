﻿#pragma once

#include <random>
#include <chrono>
#include <limits>
#include <filesystem>
#include <windows.h>

#include <glm/glm.hpp>

template<typename T>
inline T generateRandom(T min, T max)
{
    using time = std::chrono::high_resolution_clock;
    using distribution = std::conditional_t<
                                            std::is_integral<T>::value,
                                            std::uniform_int_distribution<T>,
                                            std::conditional_t<
                                                                std::is_floating_point<T>::value,
                                                                std::uniform_real_distribution<T>,
                                                                void
                                                                >
                                            >;

    std::mt19937 gen(static_cast<uint32_t>(time::now().time_since_epoch().count()));
    distribution dis(min, max);

    return dis(gen);
}

inline glm::vec1 randomVec1(float min, float max)
{
    return glm::vec1(generateRandom(min, max));
}

inline glm::vec2 randomVec2(float min, float max)
{
    return glm::vec2(randomVec1(min, max), randomVec1(min, max));
}

inline glm::vec3 randomVec3(float min, float max)
{
    return glm::vec3(randomVec2(min,max), randomVec1(min, max));
}

inline glm::vec4 randomVec4(float min, float max)
{
    return glm::vec4(randomVec3(min, max), randomVec1(min, max));
}

inline std::string getPath()
{
    char buf[1024];
    uint32_t size = sizeof(buf);
    int bytes = GetModuleFileName(NULL, buf, size);
    if (bytes == 0)
    {
        return "";
    }

    return std::experimental::filesystem::path(buf).parent_path().string();
}