﻿#pragma once

#include <string>
#include <map>
#include <GL/gl3w.h>

#include "shader.h"
#include "texture.h"

class ResourceManager
{
public:
    static Shader loadShaderFromSource(const GLchar *vertexSource, const GLchar *fragmentSource, const std::string &name);
    static Shader getShader(const std::string &name);

    static Texture loadTexture(const GLchar *file, GLboolean alpha, const std::string &name);
    static Texture getTexture(const std::string &name);

    static void clear();
private:
    ResourceManager() { }

    static std::map<std::string, Shader> shaders;
    static std::map<std::string, Texture> textures;
};