#pragma once

#include <GL/gl3w.h>
#include <glm/glm.hpp>

struct Particle
{
    glm::vec2 position;
    glm::vec2 velocity;
    glm::vec4 color;
    GLfloat life;

    Particle() : position(0.0f), velocity(0.0f), color(1.0f), life(0.0f) { }
    Particle(const glm::vec2 &position, const glm::vec2 &velocity, const glm::vec4 &color, const GLfloat &life)
        : position(position), velocity(velocity), color(color), life(life) { }
};

class ParticleManager
{
public:
    ParticleManager() : VAO_(0), VBO_(0) { }

    virtual void setup() = 0;
    virtual void update() = 0;
    virtual void draw() = 0;
    virtual void clear() = 0;
protected:
    GLuint VAO_;
    GLuint VBO_;
};