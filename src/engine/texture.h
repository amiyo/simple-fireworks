#pragma once

#include <GL/gl3w.h>

class Texture
{
public:
    Texture();

    void setInternalFormat(const GLenum &value);
    void setImageFormat(const GLenum &value);

    GLuint getID();
    GLuint getWidth() const;
    GLuint getHeight() const;

    void generate(GLuint width, GLuint height, unsigned char *data);
    void bind() const;

private:
    GLuint ID_;

    GLuint width_;
    GLuint height_;
    GLuint internalFormat;
    GLuint imageFormat;

    GLuint wrapS;
    GLuint wrapT;
    GLuint filterMin;
    GLuint filterMax;
};